package cn.uncode.springcloud.admin.controller.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

import cn.uncode.springcloud.admin.model.system.dto.CanaryDTO;
import cn.uncode.springcloud.admin.service.system.CanaryService;
import cn.uncode.springcloud.starter.web.result.P;
import cn.uncode.springcloud.starter.web.result.R;


@Controller
@RequestMapping("/canary")
public class CanaryController {

    @Autowired 
    private CanaryService canaryService;
    
    @Autowired
    private EurekaClient eurekaClient;

    
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ResponseBody
    public R<List<CanaryDTO>> getcanaryDefinitionsByVersion(){
		List<CanaryDTO> list = canaryService.getAll();
    	return R.success(list);
    }

    //添加路由信息
    @RequestMapping(value = "/add" , method = RequestMethod.POST)
    @ResponseBody
    public  R<Boolean> add(@RequestBody CanaryDTO canary){
    	long id = canaryService.add(canary);
    	if(id > 0) {
    		return R.success();
    	}
        return R.failure();
    }

    @RequestMapping(value = "/update" , method = RequestMethod.POST)
    @ResponseBody 
    public R<Boolean> edit(@RequestBody CanaryDTO canary){
    	int count = canaryService.update(canary);
    	if(count > 0) {
    		return R.success();
    	}
        return R.failure();
    }

    //打开路由列表
    @RequestMapping(value = "/list" , method = RequestMethod.GET)
    @ResponseBody
    public R<List<CanaryDTO>> list(@RequestParam int page, @RequestParam int limit){
    	P<List<CanaryDTO>> pageRt = canaryService.getPageList(page, limit);
        return R.success(pageRt).fill("count", pageRt.getRecordTotal());
    }

    @RequestMapping(value = "/delete" , method = RequestMethod.GET)
    @ResponseBody
    public  R<Boolean> delete(@RequestParam String ids){
    	if(StringUtils.isNotBlank(ids)) {
    		List<Long> idList = new ArrayList<>();
    		for(String id:ids.split(",")) {
    			if(StringUtils.isNotBlank(id)) {
    				idList.add(Long.valueOf(id));
    			}
    		}
    		int count = canaryService.delete(idList);
        	if(count > 0) {
        		return R.success();
        	}
    	}
    	
        return R.failure();
    }
    
    
    @RequestMapping(value = "/instances" , method = RequestMethod.GET)
    @ResponseBody
    public R<List<Map<String, Object>>> instances(){
    	List<Map<String, Object>> rtMap = getServiceInstance();
    	return R.success(rtMap);
    }
    
    private List<Map<String, Object>> getServiceInstance(){
    	List<Map<String, Object>> rtMap = new ArrayList<>();
		if(null != eurekaClient) {
			List<Application> apps = eurekaClient.getApplications().getRegisteredApplications();
			for(Application app : apps){
				List<InstanceInfo> instances = app.getInstancesAsIsFromEureka();
				for(InstanceInfo instance : instances){
					if(instance.getMetadata().containsKey("canaryFlag")) {
						String flag = instance.getMetadata().get("canaryFlag");
						if(StringUtils.isNotBlank(flag)) {
							Map<String, Object> obj = new HashMap<>();
							obj.put("appName", app.getName());
							obj.put("canaryFlag", flag);
							rtMap.add(obj);
						}
					}
				}
			}
		}
		return rtMap;
	}
}
