package cn.uncode.springcloud.starter.web.result;

import java.io.Serializable;

import javax.servlet.http.HttpServletResponse;

import lombok.Data;

/**
 * 业务代码枚举
 *
 * @author Juny
 */
@Data
public class ResultCode implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * code编码
	 */
	private int code;
	/**
	 * 中文信息描述
	 */
	private String message;
	
	public ResultCode(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public static ResultCode valueOf(int code, String message) {
		return new ResultCode(code, message);
	}

	public static final ResultCode IS_NULL = new ResultCode(-97, "NULL");

	/**
	 * 操作成功
	 */
	public static final ResultCode SUCCESS = new ResultCode(0, "操作成功");

	/**
	 * 业务异常
	 */
	public static final ResultCode FAILURE = new ResultCode(-98, "业务异常");

    /**
     * 参数校验失败
     */
    public static final ResultCode PARAM_VALID_ERROR = new ResultCode(-1, "参数校验失败");
	/**
	 * 请求未授权
	 */
	public static final ResultCode UN_AUTHORIZED = new ResultCode(HttpServletResponse.SC_UNAUTHORIZED, "请求未授权");

	/**
	 * 404 没找到请求
	 */
	public static final ResultCode NOT_FOUND = new ResultCode(HttpServletResponse.SC_NOT_FOUND, "404 没找到请求%name%");

	/**
	 * 消息不能读取
	 */
	public static final ResultCode MSG_NOT_READABLE = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "消息不能读取");

	/**
	 * 不支持当前请求方法
	 */
	public static final ResultCode METHOD_NOT_SUPPORTED = new ResultCode(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "不支持当前请求方法");

	/**
	 * 不支持当前媒体类型
	 */
	public static final ResultCode MEDIA_TYPE_NOT_SUPPORTED = new ResultCode(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "不支持当前媒体类型");

	/**
	 * 请求被拒绝
	 */
	public static final ResultCode REQ_REJECT = new ResultCode(HttpServletResponse.SC_FORBIDDEN, "请求被拒绝");

	/**
	 * 服务器异常
	 */
	public static final ResultCode INTERNAL_SERVER_ERROR = new ResultCode(-98, "服务器异常");

	/**
	 * 缺少必要的请求参数
	 */
	public static final ResultCode PARAM_MISS = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "缺少必要的请求参数");

	/**
	 * 请求参数类型错误
	 */
	public static final ResultCode PARAM_TYPE_ERROR = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "请求参数类型错误");

	/**
	 * 请求参数绑定错误
	 */
	public static final ResultCode PARAM_BIND_ERROR = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "请求参数绑定错误");



	

}
