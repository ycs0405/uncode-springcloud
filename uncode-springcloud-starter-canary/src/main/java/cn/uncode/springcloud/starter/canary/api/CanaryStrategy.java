package cn.uncode.springcloud.starter.canary.api;

import javax.servlet.http.HttpServletRequest;

import cn.uncode.springcloud.starter.canary.ribbon.support.RibbonContext;

public interface CanaryStrategy {
	
	String DEFAULT_STRATEGY_TYPE_IP = "ip";
	
	String DEFAULT_STRATEGY_TYPE_REQUEST = "keyInRequest";
	
	String DEFAULT_STRATEGY_TYPE_SESSION = "keyInsession";
	
	String FEIGN_CANARY_HEAD_KEY = "canary";
	
	
	RibbonContext resolve(HttpServletRequest request);
	
	

}
