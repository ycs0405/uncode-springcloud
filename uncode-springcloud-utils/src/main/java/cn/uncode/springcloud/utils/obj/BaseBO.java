package cn.uncode.springcloud.utils.obj;

import java.io.Serializable;

public interface BaseBO<T> extends Serializable{
	
	void valueFromDTO(T dto);
	
	T toDTO();

}
